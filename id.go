package fastdht

import (
	"crypto/rand"
	"encoding/hex"
	log "github.com/golang/glog"
)

type Id []byte

func tranId() string {
	b := make([]byte, 2)
	if _, err := rand.Read(b); err != nil {
		log.Fatalln("nodeId rand:", err)
	}
	return string(b)
}

func randNodeId() Id {
	b := make(Id, 20)
	if _, err := rand.Read(b); err != nil {
		log.Fatalln("nodeId rand:", err)
	}
	return Id(b)
}

func (i Id) String() string {
	return string(i)
}
func (i Id) Hex() string {
	return hex.EncodeToString(i)
}
func (i Id) Neighbor(nid string) string {
	bnid := []byte(nid)[:10]
	bi := []byte(i)[10:10]
	return string(bnid) + string(bi)
}
func Neighbor(target string, nid string) string {
	return target[:10] + nid[10:20]
}
