package main

import (
	"flag"
	"fmt"
	"git.oschina.net/xuemy/fastdht"
)

var _ = flag.Parse

func main() {
	// flag.Parse()
	out := make(chan string, 10000)

	for i := 0; i < 10; i++ {
		go func(out chan string, num int) {
			dht, _ := fastdht.NewDHT("0.0.0.0", 8999+num)
			go dht.Run(out)
		}(out, i)
	}

	// dht, _ := fastdht.NewDHT("0.0.0.0", 8999)
	// go dht.Run(out)

	for {
		select {
		case out := <-out:
			fmt.Printf("%x \n", out)
		default:

		}
	}
}
